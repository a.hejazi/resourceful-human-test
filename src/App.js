import Router from "./Router";

import './App.css';
import Header from "./components/Header/Header";

function App() {
    return (
        <>
            <Header/>
            <Router/>
        </>
    );
}

export default App;

import React from 'react';

import {Link} from "react-router-dom";
import {connect} from "react-redux";

function Header(props) {
    const {user} = props;

    const token = user?.token;
    return (
        <div className={'container-fluid mt-2 mb-2'}>
            {
                token
                    ? (
                        <Link
                            className={'btn btn-primary'}
                            to={'/profile'}>
                            Profile
                        </Link>
                    )
                    : (
                        <Link
                            className={'btn btn-primary'}
                            to={'/login'}>
                            Login
                        </Link>
                    )
            }

            <Link style={{marginLeft: '16px'}} to={'/uploader'}>
                Uploader
            </Link>
        </div>
    )

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

export default connect(mapStateToProps, null)(Header);

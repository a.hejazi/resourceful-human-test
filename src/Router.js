import React, {Component} from 'react';

import {Route, Switch, Redirect} from "react-router-dom";
import storeAndPresistor from "./redux/store";

const {store} = storeAndPresistor;

const AsyncComponent = (getComponent) => {
    return class extends Component {
        state = {
            component: null
        };

        componentDidMount() {
            getComponent().then(component => this.setState({component}))
        }

        render() {
            let Component = this.state.component;
            return Component ? <Component/> : null;
        }
    }
};

const PrivateRoute = ({component: Component, ...rest}) => (
    // protect from private routes
    <Route {...rest} render={(props) => {
        console.log(store?.getState()?.user)
        if (store?.getState()?.user?.token) {
            return <Component {...props} />
        }
        return <Redirect to='/login'/>
    }}/>
)

const Login = AsyncComponent(() => import('./pages/Login/Login').then(module => module.default));
const Profile = AsyncComponent(() => import('./pages/Profile/Profile').then(module => module.default));
const Uploader = AsyncComponent(() => import('./pages/Uploader/Uploader').then(module => module.default));
const Home = AsyncComponent(() => import('./pages/Home/Home').then(module => module.default));

const Router = () => {
    return (
        <Switch>
            <Route path={'/login'} component={Login} exact/>
            <PrivateRoute path={'/profile'} component={Profile} exact/>
            <Route path={'/uploader'} component={Uploader} exact/>
            <Route path={'/:id'} component={Home}/>
            <Route path={'/'} component={Home}/>
        </Switch>
    )
};

export default Router;

import {SET_USER, DELETE_USER} from '../actions/type';

const initialState = {
    token: null,
};

const user = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_USER:
            const {user} = action;
            return {
                token: user.token,
            };
        case DELETE_USER:
            return {
                token: null,
            };
        default:
            return state;
    }
};

export default user;

import {
    SET_USER, DELETE_USER,
} from './type';

export const setUser = (user) => ({
    type: SET_USER,
    user: user,
});

export const deleteUser = () => ({
    type: DELETE_USER,
});

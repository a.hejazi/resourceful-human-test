import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import LocalStorage from 'redux-persist/lib/storage';

import reducers from '../reducers';

const middleware = [thunk];
middleware.push(createLogger());

const persistConfig = {
    key: 'root',
    storage: LocalStorage,
    blacklist: [],
    timeout: 0,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(
    persistedReducer,
    undefined,
    compose(
        applyMiddleware(...middleware),
    ),
);

let persistor = persistStore(store);

export default {store, persistor};

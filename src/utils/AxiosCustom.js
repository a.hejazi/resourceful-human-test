import axios from 'axios';

const AxiosCustom = axios.create({
    baseURL: '',
    timeout: 30000,
    withCredentials: false,
    headers: {
        'Content-Type': 'application/json'
    }
});

AxiosCustom.interceptors.response.use((response) => response, (error) => {
    if (error.response.status === 401) {
        // clear client side authorization information
        // redirect to login page
    }
    throw error;
});

export default AxiosCustom

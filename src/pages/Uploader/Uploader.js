import React, {useRef, useState} from 'react';

import AWS from 'aws-sdk';

const S3_BUCKET = 'hejazi-test-bucket';
const REGION = 'eu-west-3';

AWS.config.update({
    accessKeyId: 'AKIAVPUOP6YAE7J2LE3U',
    secretAccessKey: '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',
})

const myBucket = new AWS.S3({
    params: {Bucket: S3_BUCKET},
    region: REGION,
})

function Uploader() {
    const [value, setValue] = useState();
    const [uploading, setUploading] = useState(false);
    const [uploadPercent, setUploadPercent] = useState(0);
    const form = useRef();

    return (
        <div className={'container'}>
            <form ref={form} onSubmit={onSubmit}>
                <input
                    className={'form-control'}
                    placeholder={'Enter your name'}
                    type={'file'}
                    accept={'video/mp4'}
                    onChange={handleOnInputChange}
                />

                {uploading && (
                    <div className="progress mt-3">
                        <div className="progress-bar" role="progressbar" style={{width: `${uploadPercent}%`}}
                             aria-valuenow="100"
                             aria-valuemin="0" aria-valuemax="100">
                            {uploadPercent === 100 ? 'please wait' : `${uploadPercent}%`}
                        </div>
                    </div>
                )}

                <button className={'btn btn-primary mt-3'} disabled={uploading}>
                    Upload
                </button>
            </form>
        </div>
    )

    function handleOnInputChange(event) {
        if (!event) {
            return;
        }
        const size = event.target.files[0].size / Math.pow(10, 6);
        if (size > 50) {
            form.current.reset();
            window.alert("file size can't more than 50MB");
            return;
        }
        setValue(event.target.files[0])
    }

    function onSubmit(e) {
        setUploading(true);

        e.preventDefault();

        uploadFile(value);
    }

    function uploadFile(file) {

        const params = {
            ACL: 'public-read',
            Body: file,
            Bucket: S3_BUCKET,
            Key: file.name
        };

        console.log(params)
        myBucket.putObject(params)
            .on('httpUploadProgress', (evt) => {
                setUploadPercent(Math.round((evt.loaded / evt.total) * 100))
            })
            .send((err) => {
                if (err) {
                    console.log(err)
                } else {
                    window.alert("uploaded successfully");
                }
                setUploading(false);
                setUploadPercent(0);
            })
    }
}

export default Uploader;

import React from 'react';

import {connect} from "react-redux";
import {Redirect, useHistory} from 'react-router-dom';

import {setUser} from "../../redux/actions";

function Login(props) {
    const {user, setUser} = props;
    const history = useHistory()

    if (user?.token) {
        return (
            <Redirect to={'/'}/>
        )
    }

    return (
        <div className={'container'}>
            <form>
                <input
                    className={'form-control'}
                    placeholder={'Enter your name'}
                />

                <button
                    className={'btn btn-primary mt-3'}
                    onClick={() => {
                        setUser({token: 'Some Token'});
                        history.push('/')
                    }}>
                    Login
                </button>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

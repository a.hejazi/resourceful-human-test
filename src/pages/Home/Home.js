import React, {useEffect, useRef, useState} from 'react';

import {useParams, useHistory} from 'react-router-dom';
import {PopupboxManager, PopupboxContainer} from 'react-popupbox';
import "react-popupbox/dist/react-popupbox.css";
import { Swiper, SwiperSlide } from "swiper/react";
import 'swiper/swiper-bundle.min.css'
import 'swiper/swiper.min.css'
import SwiperCore, {Pagination, Navigation} from 'swiper';

import styles from './Home.module.scss';
import AxiosCustom from "../../utils/AxiosCustom";

SwiperCore.use([Pagination, Navigation]);

function Home() {
    const params = useParams();
    const history = useHistory();
    const swiper = useRef();
    const [data, setData] = useState([]);
    const [lightBoxOpen, setLightBoxOpen] = useState(null);

    const imageId = params.id;
    let timeout;

    useEffect(() => {
        getData();

        return () => clearTimeout(timeout)
    }, [])

    useEffect(() => {
        handleShowLightBox();
    }, [data, imageId])

    useEffect(() => {
        if (lightBoxOpen === false) {
            history.push('/')
        }
    }, [lightBoxOpen])

    return (
        <div className={`${styles.mainContainer} row`}>

            <PopupboxContainer
                onClosed={() => setLightBoxOpen(false)}
            />

            {data.map((item, index) => {
                return (
                    <div className={`${styles.imgContainer} col-6 col-sm-1 col-md-1 col-lg-1 col-xl-1`}
                        key={item.thumbnailUrl}
                        data-testid = {`img-${index}`}>

                        <img
                            onClick={() => history.push(`/${index + 1}`)}
                            alt={item.title}
                            src={item.thumbnailUrl}
                           
                        />
                    </div>
                )
            })}
        </div>
    )

    function getData() {
        AxiosCustom('https://jsonplaceholder.typicode.com/albums/1/photos').then(res => {
            setData(res.data)
        })
    }

    function handleShowLightBox() {
        if (data.length && imageId) {
            const index = data.findIndex(el => el.id === +imageId);
            const content = (
                <div>
                    <Swiper
                        className={'swiper'}
                        onSwiper={(data) => swiper.current = data}
                        pagination={{
                            "type": "fraction"
                        }}
                        navigation={true}
                        onSlideChange={(data) => history.push(`/${data.activeIndex + 1}`)}>

                        {data.map(item => {
                            return (
                                <SwiperSlide key={item.url}>
                                    <div className={'d-flex justify-content-center'}>
                                        <img
                                            style={{maxWidth: '60%', minWidth: '250px'}}
                                            src={item.url}
                                            alt={item.title}
                                        />
                                    </div>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </div>
            )
            PopupboxManager.open({content})
            setLightBoxOpen(true)
            timeout = setTimeout(() => {
                swiper.current?.slideTo(index, 1)
            }, 10)
        }
    }
}

export default Home;

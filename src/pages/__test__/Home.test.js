import React from 'react';
import { render, screen } from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import Home from '../Home/Home';


const MockRenderImg = () =>{
  return(
    <BrowserRouter>
    <Home />
  </BrowserRouter>
  )
}

it('should render div image ', async() => {
  render(<MockRenderImg />);
  const getImg = screen.queryAllByTestId("img-0");
  expect(getImg).toBeDefined();
});



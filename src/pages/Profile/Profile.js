import React from 'react';

import {connect} from "react-redux";
import {useHistory} from "react-router-dom";

import {deleteUser} from "../../redux/actions";

function Profile(props) {
    const {deleteUser} = props;
    const history = useHistory();

    return (
        <div className={'container'}>
            <button
                className={'btn btn-danger'}
                onClick={() => {
                    deleteUser();
                    history.push('/')
                }}>
                Logout
            </button>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        deleteUser: () => {
            dispatch(deleteUser());
        },
    };
};

export default connect(null, mapDispatchToProps)(Profile);
